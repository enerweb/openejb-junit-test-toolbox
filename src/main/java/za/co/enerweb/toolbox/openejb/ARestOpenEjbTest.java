package za.co.enerweb.toolbox.openejb;

import static com.jayway.restassured.RestAssured.given;

import java.util.Properties;

import javax.ejb.embeddable.EJBContainer;

import lombok.extern.slf4j.Slf4j;

import com.google.common.base.Joiner;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;

/**
 * Tests can look like this:
 * givenApplicationUri()
 * .get(join(RCClass.URI_FRAGMENT, ADF_CONF_CLASS_SLUG))
 * .then()
 * .contentType(ContentType.JSON)
 * .root("Class",
 * withArgs(ADF_CONF_CLASS_CAPTION))
 * .body("description", equalTo(ADF_CONF_CLASS_CAPTION))
 */
@Slf4j
public abstract class ARestOpenEjbTest extends AOpenEjbTest {

    public static final String APP_NAME = "dummyApp";

    protected static Joiner PATH_JOINER = Joiner.on('/');
    protected Response response;

    static {
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
    }

    protected void setProperties(Properties properties) {
        super.setProperties(properties);
        properties.setProperty(EJBContainer.APP_NAME, APP_NAME);
    }

    protected String getApplicationUrl() {
        return getBaseUrl() + APP_NAME + "/";
    }

    protected final static String join(Object first,
        Object second,
        Object... rest) {
        return PATH_JOINER.join(first, second, rest);
    }

    protected RequestSpecification givenApplicationUri() {
        return given().log().all()
            .baseUri(getApplicationUrl());
    }

    /**
     * The main reason for this is to log the response for us but other
     * generic actions can be added even when overriding..
     */
    protected Response log(Response response) {
        this.response = response;
        log.debug(response.asString());
        // response.then().log();
        return response;
    }

    protected Response json(Response response) {
        Response ret = log(response);
        ret.then()
            .contentType(ContentType.JSON);
        return ret;
    }

    protected Response xml(Response response) {
        Response ret = log(response);
        ret.then()
            .contentType(ContentType.XML);
        return ret;
    }

    protected boolean isNotXml() {
        return !response.getContentType().equals(ContentType.XML.toString());
    }
}
