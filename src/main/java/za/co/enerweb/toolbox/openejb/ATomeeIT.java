package za.co.enerweb.toolbox.openejb;


import java.io.File;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.ejb.embeddable.EJBContainer;

import lombok.extern.slf4j.Slf4j;

/**
 * This is only suitable for integration testing because it relies on a module
 * directory which is typically the war content in the target directory..
 * Requires that you copy maven.properties to your project (and that resource
 * filtering is configured).
 */
@Slf4j
public class ATomeeIT extends AOpenEjbTest {

    protected void setProperties(Properties properties) {
        super.setProperties(properties);
        properties.setProperty(EJBContainer.PROVIDER, "tomee-embedded");

        String webappDir = getResourceBundle().getString("webappDir");
        // log.debug("webappDir=" + webappDir);
        if (!new File(webappDir).exists()) {
            throw new IllegalStateException(
                "webappDir does not exist for this integration test: "
                    + webappDir
                    + "\n (make sure `mvn package` was done first)");
        }

        properties.setProperty("javax.ejb.embeddable.modules",
            webappDir);

        // httpejbd.port seems to be ignored by tomee..
        properties.setProperty("tomee.ejbcontainer.http.port",
            "" + getHttpPort());
    }

    private ResourceBundle getResourceBundle() {
        return ResourceBundle.getBundle("maven");
    }


    public String getContextUrl() {
        return getBaseUrl()
            + getContextPath() + "/";
    }

    /**
     * no slashes
     */
    protected String getContextPath() {
        return getResourceBundle().getString("artifactId");
    }
}
