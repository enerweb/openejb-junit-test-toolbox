package za.co.enerweb.toolbox.openejb;

import static za.co.enerweb.toolbox.io.OpenPortUtils.getAvailablePort;
import static za.co.enerweb.toolbox.io.OpenPortUtils.isPortAvailable;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.ejb.embeddable.EJBContainer;
import javax.naming.Context;
import javax.naming.NamingException;

import lombok.Cleanup;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import org.apache.openejb.api.LocalClient;
import org.junit.After;
import org.junit.Before;

/**
 * Subclasses must be annotated with @org.apache.openejb.api.LocalClient
 */
@Slf4j
public abstract class AOpenEjbTest {


    private static final String META_INF_APPLICATION_CLIENT_XML =
        "META-INF/application-client.xml";
    private Context context;
    private EJBContainer container;

    private Map<String, Integer> ports = new HashMap<>();

    @Before
    @SneakyThrows
    public final void setupAOpenEjbTest() {
        ports.put("httpejbd", 8080);
        ports.put("admin", 4200);
        ports.put("ejbd", 4201);
        ports.put("ejbds", 4203);

        checkPorts();

        // needed here for users of our subclasses
        setUpBeforeInjection();
        injectContext();
        setUpAfterInjection();
    }

    @SneakyThrows
    @After
    public final void closeContext() {
        tearDownBeforeClosingContext();
        if (context != null) {
            context.close();
            context = null;
        }
        if (container != null) {
            container.close();
            container = null;
        }
        tearDownAfterClosingContext();

        for (Entry<String, Integer> entry : ports.entrySet()) {
            Integer port = entry.getValue();
            for (int i = 1; !isPortAvailable(port); i++) {
                if (i > 10) {
                    log.warn(" Port doesn't want to close :( " + entry.getKey()
                        + ".port: "
                        + port);
                    break;
                }
                if (i == 5) {
                    log.debug(i + ") waiting for " + entry.getKey()
                        + ".port to close: "
                        + port);
                }
                Thread.sleep(100 * i);
            }
        }
    }

    @SneakyThrows
    private void injectContext() {
        if (context == null) {
            inject(this);
        }
    }

    private void inject(final Object target)
        throws NamingException {
        Properties properties = new Properties();
        setProperties(properties);
        properties.list(System.out);

        container = EJBContainer.createEJBContainer(properties);
        context = container.getContext();
        try {
            if (target.getClass().getAnnotation(
                LocalClient.class) != null) {
                if (getClass().getClassLoader().getResource(
                    META_INF_APPLICATION_CLIENT_XML) == null) {
                    log.warn(META_INF_APPLICATION_CLIENT_XML
                        + " does not exist");
                } else {
                    context.bind("inject", target);
                }
            } else {
                log.warn("Subclass of AbstractEjbTest is not "
                    + "annotated with " + "@LocalClient:\n" + target);
            }
        } catch (NamingException e) {
            log.warn("Could not load context.", e);
        }
    }

    // private void initContainer(Properties properties) {
    // // try a couple of times..
    // for (int i = 1; i < 100; i++) {
    // try {
    // container = EJBContainer.createEJBContainer(properties);
    // return;
    // } catch (Exception e1) {
    // if (i < 10) {
    // log.warn("Could not instantiate container: "
    // + e1.getMessage());
    // } else {
    // throw new RuntimeException(
    // "Could not instantiate container: ", e1);
    // }
    // }
    // try {
    // Thread.sleep(100 * i);
    // } catch (InterruptedException e) {
    // log.debug("someone interrupted my sleep :(", e);
    // break;
    // }
    // }
    // }

    /**
     * You should call super if you override this..
     */
    protected void setProperties(Properties properties) {
        loadProperties(properties, "embedded-openejb.properties");

        // properties.setProperty(Context.INITIAL_CONTEXT_FACTORY,
        // "org.apache.openejb.client.LocalInitialContextFactory");
        // // Enabling option 'openejb.embedded.remotable' requires class
        // // 'org.apache.openejb.server.ServiceManager'
        URL resource = Thread.currentThread().getContextClassLoader()
            .getResource("org/apache/openejb/server/ServiceManager.class");
        if (resource != null) {
            properties.setProperty("openejb.embedded.remotable", "true");
        }
        for (Entry<String, Integer> entry : ports.entrySet()) {
            properties.setProperty(entry.getKey() + ".port", ""
                + entry.getValue());
        }

    }

    /**
     * Override this in stead of using @Before if you want to be sure that this
     * runs before injection, but call super.setUpBeforeInjection() for in case
     * one of your parents does something here.
     */
    public void setUpBeforeInjection() throws Exception {
    }

    /**
     * Override this in stead of using @Before if you want to be sure that
     * this runs after injection, but call super.setUpAfterInjection()
     * for in case one of your parents does something here.
     */
    public void setUpAfterInjection() throws Exception {
    }

    /**
     * Override this in stead of using @After if you want to be sure that this
     * runs before the context is destroyed,
     * but call super.tearDownBeforeClosingContext() for in case
     * one of your parents does something here.
     */
    public void tearDownBeforeClosingContext() throws Exception {
    }

    public void tearDownAfterClosingContext() throws Exception {
    }

    protected static void loadProperties(final Properties properties,
        final String resourcePath) {
        try {
            log.info("loading: " + resourcePath);
            ClassLoader classLoader = Thread.currentThread()
                .getContextClassLoader();
            @Cleanup InputStream resourceAsStream = classLoader
                .getResourceAsStream(resourcePath);
            if (resourceAsStream == null) {
                log.debug("Could not load properties from {}.",
                    resourcePath);
                log.debug("Thread.currentThread(): "
                    + Thread.currentThread());
                log.debug("Thread.currentThread().getName(): "
                    + Thread.currentThread().getName());
                log.debug("Thread.currentThread().getContextClassLoader(): "
                    + Thread.currentThread().getContextClassLoader());
                log.debug(
                    "EbrFactory.class.getClassLoader(): "
                        + AOpenEjbTest.class.getClassLoader());
                return;
            }

            properties.load(resourceAsStream);
        } catch (IOException e) {
            log.debug("Could not load properties from " + resourcePath
                + ".", e);
        }
    }

    private void checkPorts() {
        for (Entry<String, Integer> entry : ports.entrySet()) {
            int actualPort = getAvailablePort(entry.getValue());
            entry.setValue(actualPort);
            log.debug(entry.getKey() + ".port = " + actualPort);
        }
    }



    public int getHttpPort() {
        return ports.get("httpejbd");
    }

    public String getBaseUrl() {
        return "http://localhost:" + getHttpPort() + "/";
    }

}
