package za.co.enerweb.toolbox.openejb;

import javax.ejb.Singleton;

@Singleton
public class DummyService implements IDummyService {

    public DummyService() {
    }

    @Override
    public String getClassSimpleName() {
        return getClass().getSimpleName();
    }

}
