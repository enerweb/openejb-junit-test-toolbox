package za.co.enerweb.toolbox.openejb;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Properties;

import javax.ejb.EJB;

import org.apache.openejb.api.LocalClient;
import org.junit.Test;

/*
 * This should only be testing the thin HomeService layer.
 * This only runs with the latest home layout version.
 */
@LocalClient
public class TestAOpenEjbTest extends AOpenEjbTest {
    @EJB
    private IDummyService homeService;

    private boolean setUpBeforeInjection;
    private boolean setUpAfterInjection;

    public void setUpBeforeInjection() throws Exception {
        setUpBeforeInjection = homeService == null;
    }

    public void setUpAfterInjection() throws Exception {
        setUpAfterInjection = homeService != null;
    }

    @Override
    protected void setProperties(Properties properties) {
        super.setProperties(properties);
        // openejb normally ignores all openejb-.*
        properties.setProperty("openejb.deployments.classpath.include",
            ".*openejb-junit-test-toolbox.*");
    }

    @Test
    public void testInjection() {
        assertTrue("setUpBeforeInjection", setUpBeforeInjection);
        assertTrue("setUpAfterInjection", setUpAfterInjection);
        assertEquals("DummyService", homeService.getClassSimpleName());
    }

    @Test
    public void testInjectionCanWeDoItAgain() {
        testInjection();
    }
}
