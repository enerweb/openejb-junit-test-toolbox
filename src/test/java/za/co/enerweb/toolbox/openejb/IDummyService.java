package za.co.enerweb.toolbox.openejb;

import javax.ejb.Local;

@Local
public interface IDummyService {

    String getClassSimpleName();
}
